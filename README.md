# Lambda Simulator
[![All Contributors](https://img.shields.io/badge/all_contributors-1-orange.svg?style=flat-square)](#contributors)

Lambda Simulator solution

This project consists of a sort of load-balancer solution, and reads endpoint requests, and runs specific javascript/typescript/python/java scripts or programs such as an AWS/Azure/Other lambda, so
 you can test your lambdas in a development environment.

[Docker Repository](https://hub.docker.com/repository/docker/heatmanofurioso/lambda-simulator)

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [Deployment](#deployment) for insight on how to get your changes running in a cloud environment.

### Environment Variables

1. NODE_ENV - environment where node the lambda simulator type is decided - default value: dev
2. HTTP_PORT - http port - default value: 8080
3. HTTPS_PORT - https port. to use this, also set ENABLE_HTTPS to true, and put in your certificates in the certificates folder, if you wish - default value: 80
4. ENABLE_HTTPS - https listener - default value: false
5. ENABLE_CORS - cors for https listener - default value: false

### Usage
1. The project will launch a nodeJS lambda simulator, running on the root folder /app, which is the lambda simulator
2. The lambda simulator will listen to http requests to any endpoint configured on it's lambda-scripts file. The file is located inside /app/assets/lambda-scripts.json
3. Add your lambda scripts, or projects into the /projects/ root folder
4. If you wish to run the lambda simulator in production mode with transpiled javascript, change the docker entrypoint from dev to prod, or just build the project yourself with `npm run prod`

#### Prerequisites

1. Docker

#### Installation

1. Clone the repository
2. Open an elevated command prompt on the cloned folder
3. Run docker run /dockerfiles/Dockerfile -p 8080:8080

#### Version Bump

Please refer to the [Versioning Convention](#versioning-convention) section on how the SemVer specification works.

In this project, version bumping is achieved using the `npm version` command. Use the following guidelines to decide to which version to bump, bearing in mind the SemVer specification:

* When you make incompatible API changes, use `npm version major`
* When you add functionality in a backwards-compatible manner, use `npm version minor`
* When you make backwards-compatible bug fixes, use `npm version patch`
* When you are developing but do not want to make official releases (alpha or beta versions, for example), use `npm version prerelease`
* When you want to bump to a specific version number (represented by X), use `npm version X`

If your changes affect only documentation, unit tests, or other areas that do not concern code, do not bump the version.

There is a script which suggests the version bump. To use it, run `npm run recommended-bump`. However, bear in mind that it only outputs `major`, `minor` and `patch`. You must consider whether you want to bump the version at all or use a `prerelease` version number regardless of the result of the `recommended-bump` script.

#### Merge Requests

The master branch is protected against pushes, and therefore all commits to master must be made via merge requests. When a merge request is ready to be merged into master, it is important that the version is bumped according to the previous section. Running this command ensures all tests are run, and the `CHANGELOG.md` file is updated.

#### Contributors

Thanks goes to these wonderful people ([emoji key](https://github.com/kentcdodds/all-contributors#emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore -->
| [<img src="https://gitlab.com/uploads/-/system/user/avatar/747737/avatar.png?width=400" width="100px;"/><br /><sub><b>Tiago Casinhas</b></sub>](https://gitlab.com/Heatmanofurioso)<br />[💻]<br/>
| :---: | :---: | :---: | :---: | :---: | :---: |
<!-- ALL-CONTRIBUTORS-LIST:END -->

This prhttps://secure.gravatar.com/avatar/abeae362f7f741797ffaa7d100e20e8c?s=180&d=identiconoject follows the [all-contributors](https://github.com/kentcdodds/all-contributors) specification. Contributions of any kind welcome!

To add yourself as contributor, please run `npx all-contributors add` and follow the instructions.

#### Versioning Convention

This project follows the [Semantic Versioning 2.0.0](https://semver.org/) guide. Never create versions that are not SemVer compliant.

It is important that the version in the `package.json` file matches the snapshot name in IBM BPM. This ensures that all releases will have a corresponding tag created, so that the code is easily traced. Besides, the build process will embed the version number in the javascript file.

#### Branch Naming Conventions

When creating new branches, please stick to the following naming conventions:
* `experiment/*` for branches that are intended for experiments and are meant to be discarded
* `feature/*` for branches that will contain new features
* `bugfix/*` for branches that will fix any existing bugs
* `test/*` for branches that will affect unit tests only
* `refactor/*` for branches that will change the code in a non functional way only
* `docs/*` for branches that will affect documentation only
* `chore/*` for branches that will not affect neither code nor documentation (e.g. `.gitlab-ci.yml` or lint rules)

These naming conventions take precedence over each other in the order they are presented. For example, you should consider whether your branch name should start with `feature/` before considering if it should start with `test/`. A branch that contains both a new feature and unit tests should follow the `feature/*` convention. A branch that has an experimental feature and will never be merged should follow the `experiment/*` regardless of implementing new features or fixing bugs.

### Roadmap
1. Improve documentation.
2. Add toggeable metrics collection, such as time to execute and resources spent, so you can calculate how your lambda would perform on the cloud
3. Improve documentation

### My stacks
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/Heatmanofurioso/)
