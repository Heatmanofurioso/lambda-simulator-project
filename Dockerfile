FROM openjdk:15.0.1-slim

EXPOSE 8080
EXPOSE 80

RUN mkdir /app
RUN mkdir /projects

RUN apt-get update -y

RUN apt-get install wget -y

RUN wget -qO- https://deb.nodesource.com/setup_15.x | bash -

RUN apt-get install software-properties-common -y

RUN apt-get update -y

RUN apt-get install python -y

RUN apt-get install python3-pip -y

RUN apt-get install nodejs -y

COPY LambdaSimulator/ /app/

COPY Lambda-Projects/ /projects/

WORKDIR /app

RUN npm install

RUN npm run tsc

ENTRYPOINT ["npm", "start", "dev"]
