import winston, {LoggerOptions} from 'winston';

const loggerOptions: LoggerOptions = {
    level: 'info',
    format: winston.format.json(),
    defaultMeta: {service: 'lambda-simulator-service'},
    transports: [
        new winston.transports.File({filename: 'error.log', dirname: 'logging', level: 'error'}),
        new winston.transports.File({filename: 'combined.log', dirname: 'logging'})
    ]
};

const logger = winston.createLogger(loggerOptions);
// Configure logger settings
logger.remove(winston.transports.Console);
logger.add(new winston.transports.Console({
    format: winston.format.simple()
}));
logger.level = 'debug';

export default logger;
