import https from 'https';
import http from 'http';
import fs from 'fs';
import cors from 'cors';
import dotenv from 'dotenv';
import {exec} from 'child_process';
import express from 'express';
import bodyParser from 'body-parser';
import util from 'util';
import lambdaScripts from '../assets/lambda-scripts.json';
import logger from './logger';

dotenv.config();
const app = express();
const httpPort = process.env.HTTP_PORT;
const httpsPort = process.env.HTTPS_PORT;

app.use(bodyParser.json());
app.use(function (req: any, res: any, next: any) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

lambdaScripts.forEach(function (eachScript: any) {
    // define a route handler for the default home page
    app.get(eachScript.path, (req: any, res: any) => {

        logger.info('Processing endpoint:'.concat(eachScript.path));

        let requestBody = req.body;
        if (eachScript.extraJSONargs != null) {
            requestBody = {
                ...requestBody,
                ...eachScript.extraJSONargs
            };
        }
        logger.info('With message:'.concat(JSON.stringify(requestBody)));
        const child = exec(eachScript.script.concat(JSON.stringify(requestBody)));
        let response = {};
        let responseCode: '200';
        child.stdout.on('data', function (data: any) {
            console.log('stdout: ==== ' + data);
            response = data;
        });
        child.stderr.on('data', function (data: any) {
            console.log('stdout: ' + data);
        });
        child.on('close', function (code: any) {
            console.log('closing code: ' + code);
            responseCode = code;
        });
        child.on('message', function (msg: any) {
            console.log('message from child: ' + util.inspect(msg));
        });
        res.send({
            response: response,
            statusCode: responseCode
        });
    });
});


// start the Express server
if (JSON.parse(process.env.ENABLE_CORS)) {
    app.use(cors());
}
if (JSON.parse(process.env.ENABLE_HTTPS)) {

    const privateKey = fs.readFileSync('assets/sslcert/server.key', 'utf8');
    const privateCertificate = fs.readFileSync('assets/sslcert/server.crt', 'utf8');
    const credentials = {
        key: privateKey,
        cert: privateCertificate
    };

    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(httpsPort, () => {
        console.log(`Server started at https://localhost:${httpPort}`);
    });
}
const httpServer = http.createServer(app);
httpServer.listen(httpPort, () => {
    console.log(`Server started at http://localhost:${httpPort}`);
});
export {};
